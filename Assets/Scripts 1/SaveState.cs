﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SaveState 
{
    public List<string> inventory;
    public string currentRoom;   
}
